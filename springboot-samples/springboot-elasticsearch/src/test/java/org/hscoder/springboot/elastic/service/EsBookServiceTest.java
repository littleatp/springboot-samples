package org.hscoder.springboot.elastic.service;

import org.assertj.core.util.DateUtil;
import org.hscoder.springboot.elastic.ElasticBoot;
import org.hscoder.springboot.elastic.domain.EsBook;
import org.hscoder.springboot.elastic.repository.EsBookRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ElasticBoot.class)
public class EsBookServiceTest {

    @Autowired
    private EsBookService bookService;

    @Autowired
    private EsBookRepository bookRepository;

    @Before
    public void initData() {
    }

    @After
    public void disposeData() {
        bookRepository.deleteAll();
    }

    @Test
    public void testCreateBook() {
        EsBook book = bookService.createEsBook("庞古丽", "现代小说", "一念天堂，一念地狱",
                Arrays.asList("悬疑", "剧情"), DateUtil.parse("2015-11-21"));
        assertNotNull(book);

        book = bookService.getEsBook(book.getBookId());
        assertNotNull(book);
    }

    @Test
    public void testDeleteBook() {

        EsBook book = bookService.createEsBook("庞古丽", "文学", "现代文学基础讲义", Arrays.asList("悬疑", "剧情"), new Date());
        assertNotNull(book);

        assertTrue(bookService.deleteEsBook(book.getBookId()));
        assertFalse(bookService.deleteEsBook(book.getBookId()));

    }

    @Test
    public void testUpdateBook() {

        EsBook book = bookService.createEsBook("庞古丽", "文学", "现代文学基础讲义", Arrays.asList("悬疑", "剧情"), new Date());
        assertNotNull(book);

        boolean flag = bookService.updateEsBook(book.getBookId(), book.getType(), "隋唐传");
        assertTrue(flag);

        EsBook newbook = bookService.getEsBook(book.getBookId());
        assertNotNull(newbook);
        assertTrue(book.getType().equals(newbook.getType()));
        assertNotEquals(book.getTitle(), newbook.getTitle());
    }

    @Test
    public void testUpdateTags() {

        EsBook book = bookService.createEsBook("庞古丽", "文学", "现代文学基础讲义", Arrays.asList("悬疑", "剧情"), new Date());
        assertNotNull(book);

        boolean flag = bookService.updateTags(book.getBookId(), Arrays.asList("校园", "言情"));
        assertTrue(flag);

        EsBook newbook = bookService.getEsBook(book.getBookId());
        assertNotNull(newbook);
        assertTrue(newbook.getTags().size() == 2);

        List<EsBook> books = bookService.listByTag(Arrays.asList("悬疑"));
        assertTrue(books.size() == 0);

        books = bookService.listByTag(Arrays.asList("言情", "喜剧"));
        assertTrue(books.size() == 1);

        books = bookService.listByTag(Arrays.asList("喜剧"));
        assertTrue(books.size() == 0);
    }

    @Test
    public void testListTop10ByType() {

        bookService.createEsBook("庞古丽", "文学", "现代文学基础讲义", Arrays.asList("悬疑", "剧情"), DateUtil.parse("2015-11-21"));
        bookService.createEsBook("程晓", "历史", "大唐兴衰史", Arrays.asList("战争", "剧情"),DateUtil.parse("2015-09-21"));
        bookService.createEsBook("庞古丽", "历史", "晚清后宫故事", Arrays.asList("后宫", "王权"), DateUtil.parse("2017-03-21"));


        List<EsBook> books = bookService.listTop10ByType("历史");

        assertTrue(books.size() == 2);
        assertEquals("晚清后宫故事", books.get(0).getTitle());
    }

    @Test
    public void testSearch() {

        bookService.createEsBook("庞古丽", "文学", "现代文学基础讲义", Arrays.asList("悬疑", "剧情"), DateUtil.parse("2015-11-21"));
        bookService.createEsBook("程晓", "历史", "大唐兴衰史", Arrays.asList("战争", "剧情"),DateUtil.parse("2015-09-21"));
        bookService.createEsBook("庞古丽", "历史", "论新世纪的国民现状", Arrays.asList("后宫", "王权"), DateUtil.parse("2017-03-21"));
        bookService.createEsBook("林文君", "纪实", "全球暖化现状研究", Arrays.asList("政治", "物理"), DateUtil.parse("2018-11-09"));
        bookService.createEsBook("李秀英", "纪实", "国学兴衰史", Arrays.asList("文化", "政治"), DateUtil.parse("2017-12-11"));

        PageRequest pageRequest = PageRequest.of(0, 10, Sort.by(Sort.Direction.DESC, "publishDate"));

        Page<EsBook> books = bookRepository.search(null, null, null, null, pageRequest);
        assertTrue(books.getTotalElements() == 5);


        books = bookRepository.search("历史", "庞古丽", null, null, pageRequest);
        assertTrue(books.getTotalElements() == 1);

        books = bookRepository.search("", "", null, Arrays.asList("政治"), pageRequest);
        assertTrue(books.getTotalElements() == 2);

        books = bookRepository.search("", "", "兴衰", null, pageRequest);
        assertTrue(books.getTotalElements() == 2);

        books = bookRepository.search("", "", "现", null, pageRequest);
        assertTrue(books.getTotalElements() == 3);
        assertEquals(books.getContent().get(0).getAuthor(), "林文君");
    }

}
