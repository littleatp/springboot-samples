package org.hscoder.springboot.hbase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HBaseBoot {

    public static void main(String[] args) {
        SpringApplication.run(HBaseBoot.class);
    }
}
